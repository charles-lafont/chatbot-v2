const server = require('http').createServer();
const io = require('socket.io-client')(server);

io.on('connection', (Socket) => {
  alert('An user has arrived');
  Socket.send('Hello');

  Socket.on('disconnect', () => {
    alert('An user has left');
  });

  Socket.on('Chat', (msg) => {
    console.log(msg);
  });
});
