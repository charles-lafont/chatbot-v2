const Bot = class Bot {
  constructor(id, name, avatar) {
    this.id = id;
    this.name = name;
    this.avatar = avatar;
    this.commands = [];
  }

  myCommands(command) {
    this.commands.push(command);
  }
};

export default Bot;
