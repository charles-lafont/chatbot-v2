const Chat = class Chat {
  constructor() {
    this.el = document.querySelector('#app');
    this.bots = [];
    this.run();
    localStorage.setItem('messages', '[]');
  }

  myBots(type) {
    this.bots = this.bots.concat(type);
  }

  process(selected) {
    const currentBot = selected;
    switch (currentBot.id) {
      case 1:
        this.hello(currentBot);
        break;
      default:
              // do nothing
    }
  }

  /**
         * Render Message Send
         * @param {message} message
         * @returns
         */
  renderMessageSend(message) {
    const date = new Date();
    const { avatar, text } = message;
    return `
    <div class="row">
      <div class="message-sent">
        <img src="${avatar}">
        <p class="text">${text}</p>
        <p>${date.getHours()}:${date.getMinutes()}</p>
      </div>
    </div>
    `;
  }

  /**
         * Render Messasge Received
         * @param {message} message
         * @returns
         */
  renderMessageReceived(id, message) {
    const date = new Date();
    const { avatar, text } = message;

    return `
            <div class="row">
              <div class="message-received">
                <img src="${avatar}" alt="Profil pic">
                <p class="text">${text}</p>
                <span>${date.getHours()}:${date.getMinutes()}</span>
              </div>
            </div>
                `;
  }

  /**
         * Render Messages
         * @returns
         */
  renderMessages() {
    return `        
        <div class="col-9 right">
            <div class="head">
                <p>TO : Bots</p>
                <span>online : 1 min ago</span>
            </div>
            <hr>

            <div class="messages">
          
            </div>

            ${this.renderTypingMessage()}
        </div>
             
                `;
  }

  /**
         * Render Typing Message
         * @returns TypingMessage
         */
  renderTypingMessage() {
    return `
        <div class="typing-message">
            <input type="text" class="myInput" id="input">
            <p>Send</p>
        </div>
 
                `;
  }

  /**
         * Render Contact
         * @param {contact} contact
         * @returns
         */
  renderContact(contact) {
    const { avatar, name } = contact;
    return `
            <div class="profil" id="${name}">
              <img src="${avatar}" alt="Profil pic">
              <p>${name}</p>
              <span class="online">online</span>
              <p class="greenpoint"></p>
            </div>
                `;
  }

  /**
         * Render Contacts
         * @param {*} contacts
         * @returns
         */
  renderContacts(contacts) {
    return `  
            <div class="col-3 left">
                <h1 class="text-center">BIKINI BOTTOM</h1>
                ${contacts.map((contact) => this.renderContact(contact)).join('')}
            </div> 
                `;
  }

  /**
         * Typing Message
         */
  typingMessage() {
    const el = document.querySelector('.typing-message input');
    const messagesEl = document.querySelector('.messages');

    el.addEventListener('keypress', (e) => {
      if (e.keyCode === 13) {
        const text = e.currentTarget.value;

        const message = {
          author: 'Charles',
          avatar: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/0045db5a-3a85-434a-afa5-45944bfd5327/dba0z41-d3d9f0f1-e694-4558-a586-f7dc4e73fb11.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzAwNDVkYjVhLTNhODUtNDM0YS1hZmE1LTQ1OTQ0YmZkNTMyN1wvZGJhMHo0MS1kM2Q5ZjBmMS1lNjk0LTQ1NTgtYTU4Ni1mN2RjNGU3M2ZiMTEuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.J9-ZAuCSsvFkf_L5htd2TCnSAvsLPPSdQigqK0SZ3_o',
          text
        };

        e.currentTarget.value = '';

        messagesEl.innerHTML += this.renderMessageSend(message);
      }
    });

    this.setHistoryMessage();
    window.scrollTo(0, document.body.scrollHeight);
  }

  setHistoryMessage() {
    const messages = this.getHistoryMessage();

    messages.push(messages);

    localStorage.setItem('messages', JSON.stringify(messages));
  }

  run() {
    const contacts = [{
      name: 'Bob L\'éponge',
      avatar: 'https://avatarfiles.alphacoders.com/152/152177.jpg'
    }, {
      name: 'Carlo',
      avatar: 'https://i.pinimg.com/280x280_RS/fc/20/d7/fc20d7ba75499df5113b2b3f94b87730.jpg'
    }, {
      name: 'Patrick',
      avatar: 'https://avatarfiles.alphacoders.com/165/thumb-165966.jpg'
    }];
    this.el.innerHTML += this.renderContacts(contacts);
    this.el.innerHTML += this.renderMessages();
    this.typingMessage();
  }
};

export default Chat;
