import './index.scss';
import Chat from './chat';
import Bot from './bot';

const chat = new Chat();

const spongeBob = new Bot(1, 'Bob', 'https://avatarfiles.alphacoders.com/152/152177.jpg');

spongeBob.myCommands('Hello', 'hello');

chat.myBots(spongeBob);

chat.run();
